
import pyvista as pv  # type: ignore
import plegma.project as p
import plegma.simplify as simp  # type: ignore
import plegma.generate as g  # type: ignore
import common.seg as seg
import common.L1L2_cells_ids as layers
from pyvista.core.pointset import PolyData  # type: ignore
from common.seg import STissue, Cell
import common.statesN as s
from typing import List, Tuple, TypeVar, Callable, Dict, Any
import os
from functools import partial

T = TypeVar('T')
State = int


class MechCell():

    def __init__(self, conc, extra=[]):
        self.mt = [1.0, 0.0, 0.0, 1.0]
        self.stress = [1.0, 0.0, 0.0, 1.0]
        self.strain = [1.0, 1.0, 0.0, 1.0]
        self.e = [1.0, conc, 0.0]
        self.zc = 0.0
        self.yl = 0.0
        self.strain_a = 0.0
        self.stress_a = 0.0
        self.area_rat_ind = 0.0
        self.iso_energy_ind = 0.0
        self.aniso_energy_ind = 0.0
        self.mt_stress_ind = 1.0
        self.vel_store_ind = 0.0
        self.mt_stress_ang = 0.0
        self.stress_strain_ang = 0.0
        self.vol_store_ind = 0.0
        self.stress_tensor = [[0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0]]
        self.normal = [0.0, 0.0]
        self.extra = extra

    def to_init(self):
        return (" ".join([str(p) for p in self.mt] +
                         [str(p) for p in self.stress] +
                         [str(p) for p in self.stress] +
                         [str(p) for p in self.e] +
                         [str(self.zc)] +
                         [str(self.yl)] +
                         [str(self.strain_a)] +
                         [str(self.stress_a)] +
                         [str(self.area_rat_ind)] +
                         [str(self.iso_energy_ind)] +
                         [str(self.aniso_energy_ind)] +
                         [str(self.mt_stress_ind)] +
                         [str(self.vel_store_ind)] +
                         [str(self.mt_stress_ang)] +
                         [str(self.stress_strain_ang)] +
                         [str(self.vol_store_ind)] +
                         [str(el) for p in self.stress_tensor for el in p] +
                         [str(p) for p in self.normal] +
                         [str(e) for e in self.extra]))


def yts_st_regions(st: Dict[str, Any]) -> MechCell:
    state_cells = {16: MechCell(1.0),
                   18: MechCell(1.0),
                   27: MechCell(1.0),
                   28: MechCell(1.0),
                   31: MechCell(0.0)}

    return state_cells.get(st["state"], MechCell(0.5))


def bexpr_regions(g1: str, g2: str, st: Dict[str, bool]) -> MechCell:
    def getTopExpr(e):
        return e.body[0].value

    e1 = getTopExpr(seg.ge("'{g1}' and '{g2}'".format(g1=g1, g2=g2)))
    e2 = getTopExpr(seg.ge("'{g1}' and not '{g2}'".format(g1=g1, g2=g2)))

    if seg.evalB(e1, st):
        print(0.0)
        return MechCell(100.0)
    elif seg.evalB(e2, st):
        print(1.0)
        return MechCell(0.0)
    else:
        print(0.5)
        return MechCell(0.5)


region_fs = [partial(bexpr_regions, g1='ATML1', g2='AHP6'),
             partial(bexpr_regions, g1='ETTIN', g2='ANT'),
             partial(bexpr_regions, g1='AP1', g2='AHP6'),
             partial(bexpr_regions, g1='MP', g2='FIL'),
             partial(bexpr_regions, g1='AP1', g2='ANT'),
             partial(bexpr_regions, g1='ETTIN', g2='AHP6')]


def to_init(mesh: PolyData,
            to_cell: Callable[[Dict[str, Any]], MechCell],
            init_out: str = "out.init"):

    converter_bin = "~/tissue/bin/converter"
    ply_in = "test.ply"
    
    mesh.extract_surface().save(ply_in, binary=False)

    # convert geometry using Tissue converter
    cmd = "{bin} -input_format ply {fin} > {fout}"
    os.system(cmd.format(bin=converter_bin,
                         fin=ply_in,
                         fout=init_out))

    # convert cell info
    ks = mesh.cell_arrays.keys()
    n_cells = mesh.n_cells

    init_cells = list()
    for i in range(n_cells):
        env = {k: mesh.cell_arrays[k][i] for k in ks}
        init_cells.append(to_cell(env))

    # add cell info to init file
    with open(init_out, "a") as finit:
        finit.write("\n".join([c.to_init() for c in init_cells]))


ToCellF = Callable[[Dict[str, Any]], MechCell]


def to_init_ms(mesh: PolyData,
               fs: List[ToCellF],
               init_out="out.init"):

    for i, f in enumerate(fs):
        fn = "out{i}.init".format(i=i)
        to_cell_info(mesh, f, fn)


def to_cell_info(mesh: PolyData,
                 to_cell: Callable[[Dict[str, Any]], MechCell],
                 init_out: str = "out.init"):

    # convert cell info
    ks = mesh.cell_arrays.keys()
    n_cells = mesh.n_cells

    init_cells = list()
    for i in range(n_cells):
        env = {k: True if mesh.cell_arrays[k][i] == 1 else False for k in ks}
        init_cells.append(to_cell(st=env))

    # add cell info to init file
    with open(init_out, "a") as finit:
        finit.write("\n".join([c.to_init() for c in init_cells]))


def to_eply(st_mesh):
    pass


def scale(mesh: PolyData,
          res: List[float]) -> PolyData:
    mesh_scaled = pv.PolyData(mesh)
    mesh_scaled.points[:, [0, 2]] = mesh_scaled.points[:, [2, 0]]
    mesh_scaled.points = mesh_scaled.points * res

    return mesh_scaled


def get_mesh_seg_atlas(t: int) -> Tuple[PolyData, STissue]:
    im_loc = "data/segmentation_tiffs/{t}h_segmented.tif".format(t=str(t))
    im = g.read_image(im_loc)
    ress = seg.readRess("data/resolutions.txt")
    res = ress[t]

    mesh = g.mk_mesh(im, level=1)
    mesh = simp.remesh(mesh, 5000)

    mesh = scale(mesh, res)

    ts = seg.readDataT1Im(d="data", dExprs="data/geneExpression", t=t)

    return mesh, ts


def extract_L1(mesh: PolyData, ts: STissue, t: int) -> PolyData:
    mesh_ = p.project_seg(mesh, ts, lambda c: c.cid, 'label')
    mesh__ = p.extract_by_label(mesh_, layers.L1[t] + layers.L2[t])

    ms = pv.PolyData(mesh__.points, mesh__.cells)

    return ms


def add_sts_info(mesh: PolyData, ts: STissue) -> PolyData:
    def getStCell(c: Cell) -> int:
        return c.getState(s.states)

    mesh_ = p.project_seg(mesh, ts, getStCell, 'state')

    return mesh_


def add_gene_state_info(mesh: PolyData, ts: STissue) -> PolyData:
    from functools import partial

    def get_expr(c: Cell, geneNm: str) -> int:
        return int(c.exprs.get(geneNm, False))

    for gn in seg.geneNms:
        print(gn)
        mesh = p.project_seg(mesh, ts, partial(get_expr, geneNm=gn), gn)

    return mesh


def go_states_atlas(t: int) -> PolyData:
    m, ts = get_mesh_seg_atlas(t)
    m_L1 = extract_L1(m, ts, t)

    ms = add_sts_info(m_L1, ts)

    return ms


def go_atlas_L1(t: int) -> PolyData:
    m, ts = get_mesh_seg_atlas(t)
    m_L1 = extract_L1(m, ts, t)

    return m_L1


def write_atlas_states_at(t: int):
    m_st = go_states_atlas(t=t)

    init_fout = "{t}h_sts.init".format(t=str(t))
    to_init(m_st, yts_st_regions, init_fout)
