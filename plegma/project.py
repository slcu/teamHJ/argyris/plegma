from scipy.spatial import KDTree  # type: ignore
import numpy as np  # type: ignore
from pyvista.core.pointset import PolyData  # type: ignore
from typing import TypeVar, List, Tuple, Generic, Callable, Dict
from common.seg import STissue, Tissue, Cell

T = TypeVar('T')
U = TypeVar('U')

Point3D = Tuple[float, float, float]


class LabPolyData(Generic[T]):
    mesh: PolyData

    def __init__(self, m: PolyData) -> None:
        self.mesh = m

    def cell_arrays(self):
        return self.mesh.cell_arrays

    def extract_cells(self, inds):
        return self.mesh.extract_cells(inds)

    def extract_surface(self):
        self.mesh.extract_surface()

    def save(self, fn):
        self.mesh.save(fn)


def cell_centre(vs):
    return sum(vs) / 3.0


# def cell_poss(mesh):
#     for i in range(0, (len(mesh.faces)), 4):
#         print(i)
#         f = mesh.faces[i:i+4]
#         yield (mesh.points[f[-3:][0]],
#                mesh.points[f[-3:][1]],
#                mesh.points[f[-3:][2]])


def cell_poss(mesh):
    return list(mesh.cell_centers().points)


def mk_cmap(im, res, f=lambda c: True):
    cmap = []
    it = np.nditer(im, flags=['multi_index'])

    while not it.finished:
        print(it.multi_index[0])
        pos = np.multiply(np.array(it.multi_index), np.array(res))
        val = im[it.multi_index]

        if f((pos, val)):
            cmap.append((pos, val))
            
        it.iternext()

    return cmap


def mk_cmap_seg(ts: STissue,
                f: Callable[[Cell], U]) -> List[Tuple[Point3D, U]]:
    return [((c.pos.x, c.pos.y, c.pos.z), f(c)) for c in ts]


def mk_query_cmap(cmap: List[Tuple[Point3D, U]]) -> Tuple[KDTree,
                                                          Dict[int, U]]:
    poss = []
    signal: Dict[int, U] = {}
    for i, (pos, val) in enumerate(cmap):
        poss.append(pos)
        signal[i] = val

    return KDTree(poss), signal


def project_seg(mesh: PolyData,
                ts: STissue,
                f: Callable[[Cell], U],
                name: str) -> PolyData:
    points, vals = mk_query_cmap(mk_cmap_seg(ts, f))
    cell_ctrs = cell_poss(mesh)

    _, iss = points.query(cell_ctrs)

    mesh.cell_arrays[name] = np.array([vals[i] for i in iss])

    return mesh


def get_poss_array(ts: Tissue):
    return np.array([[c.pos.x, c.pos.y, c.pos.z]
                     for c in ts])


def map_segs(ts: STissue, ts_: STissue):
    poss = get_poss_array(ts)
    poss_ = get_poss_array(ts_)
    
    poss = [(c.pos.x, c.pos.y) for c in ts]
    for i, (pos, val) in enumerate(cmap):
        poss.append(pos)
        signal[i] = val

    return KDTree(poss), signal
    


def extract_by_label(lab_mesh: PolyData, ids: List[int]):
    sids = set(ids)
    tids = [i for i, lb in enumerate(lab_mesh.cell_arrays['label'])
            if lb in sids]

    return lab_mesh.extract_cells(tids)
