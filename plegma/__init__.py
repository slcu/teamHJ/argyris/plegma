# -*- coding: utf-8 -*-

"""Top-level package for Phenotastic."""
import warnings

import os
import sys
import vtk
import numpy as np

__author__ = """Henrik Ahl"""
__email__ = 'hpa22@cam.ac.uk'
__version__ = '0.1.1'
