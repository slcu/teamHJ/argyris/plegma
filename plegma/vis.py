import numpy as np  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import pyvista as pv  # type: ignore
import matplotlib  # type: ignore


class IndexTracker(object):
    def __init__(self, ax, X):
        self.ax = ax
        ax.set_title('use scroll wheel to navigate images')

        self.X = X
        self.slices, rows, cols = X.shape
        self.ind = self.slices//2

        self.im = ax.imshow(self.X[self.ind, :, :])
        self.update()

    def onscroll(self, event):
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        self.im.set_data(self.X[self.ind, :, :])
        self.ax.set_ylabel('slice %s' % self.ind)
        self.im.axes.figure.canvas.draw()


def plot3d(data):
    fig, ax = plt.subplots(1, 1)
    tracker = IndexTracker(ax, data)
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()


def rand_cmap():
    cmap = matplotlib.colors.ListedColormap(np.random.rand(256, 3))
    return cmap


def state_cmap():
    return matplotlib.colors.ListedColormap(['blue', 'red'])


def show_labmesh(mesh, nm='label', cmap=rand_cmap()):
    plotter = pv.Plotter()
    plotter.add_mesh(mesh, scalars=nm, cmap=cmap)
    plotter.show()
