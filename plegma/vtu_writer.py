from typing import List, TypeVar, Tuple, FrozenSet, Dict
from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np

T = TypeVar('T')

Edge = FrozenSet[int]


def head(xs: List[T]) -> T:
    if xs:
        return xs[0]
    else:
        raise Exception("")


def tail(xs: List[T]) -> List[T]:
    return xs[1:]


def read_head(ns: List[int]) -> Tuple[int, List[int]]:
    return (head(ns), tail(ns))


def read_vertices(ns: List[int],
                  n: int,
                  ps) -> Tuple[List[FrozenSet[int]], List[int]]:
    verts = list()
    rest = ns
    for i in range(n):
        v, rest = read_head(rest)
        verts.append(v)

    ps_ = np.array([ps[v, :] for v in verts])
    inds, _ = sort_verts(ps_)
    verts_ = get_circ_cons([verts[i] for i in inds])

    edges = [frozenset([v1, v2]) for v1, v2 in zip(verts_[::2], verts_[1::2])]

    return (edges, rest)


def mk_ts_(ns: List[int],
           ncells: int,
           ps,
           cs: Dict[int, List[Edge]]) -> Dict[int, List[Edge]]:
    while True:
        if not ns:
            print(ncells)
            break
        else:
            print(ncells)
            car, rest = read_head(ns)
            edges, rest_ = read_vertices(rest, car, ps)
            cs[ncells] = edges
            ncells += 1
            ns = rest_

    return cs


def mk_ts(ns: List[int], ps):
    return mk_ts_(ns, 0, ps, dict())


def invert_dlist(cs: Dict[int, List[Edge]]) -> Dict[Edge, List[int]]:
    d = defaultdict(list)

    for cid, es in cs.items():
        for e in es:
            d[e].append(cid)

    return d


def iter_repr(xs):
    return " ".join([str(x) for x in xs])


def wrepr(i, k, cids):
    return " ".join([str(i), iter_repr(k), iter_repr(cids)])


def grid_to_init(grid):
    walls = invert_dlist(mk_ts(list(grid.cells)))

    # print walls
    return "\n".join([wrepr(i, k, walls[k])
                      for i, k in enumerate(list(walls.keys()))])


def plot_2d_annot(ps):

    xs = ps[:, 0]
    ys = ps[:, 1]

    plt.plot(xs, ys, 'o')
    for i, xy in enumerate(zip(xs, ys)):
        plt.annotate(str(i), xy)

    plt.show()


def sort_verts(ps):

    def angle(p, c):
        return (np.arccos(np.dot(p, c) /
                          (np.linalg.norm(p)*np.linalg.norm(c))))

    c = np.mean(ps, axis=0)
    inds = np.argsort([angle(p, c) for p in list(ps)])

    return inds, ps[inds, :]


def get_circ_cons(xs):
    return (list(sum(list(zip(xs, xs[1:])), ())) +
            [xs[-1], xs[0]])


def get_edge_coords(ps):
    xs = ps[:, 0]
    ys = ps[:, 1]
    zs = ps[:, 2]

    xs_ = get_circ_cons(xs)
    ys_ = get_circ_cons(ys)
    zs_ = get_circ_cons(zs)

    return xs_, ys_, zs_


def plot_3d_annot(ps):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    xs = ps[:, 0]
    ys = ps[:, 1]
    zs = ps[:, 2]

    ax.scatter(xs, ys, zs)

    for i, xyz in enumerate(zip(xs, ys, zs)):
        x, y, z = xyz
        ax.text(x, y, z, str(i))

    xs_, ys_, zs_ = get_edge_coords(sort_verts(ps))

    ax.plot(xs_, ys_, zs_)

    plt.show()
