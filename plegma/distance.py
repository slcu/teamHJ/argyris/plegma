import numpy as np  # type: ignore
import pyvista as pv  # type: ignore
from scipy.spatial import KDTree  # type: ignore
from scipy.linalg import lstsq  # type: ignore


def idx1(x): return x[0]


def idx2(x): return x[1]


def dist(x): return x[2]


def map_points(source, target):
    n = len(source)
    tt = KDTree(target)

    ds, idxs = tt.query(source)

    return zip(list(np.arange(n)), list(idxs), list(ds))


def centre_points(X):
    return X - np.mean(X, axis=0)


def icp(source, target, trim=0.5, maxIter=1000, eps=0.005):
    A = np.random.rand(3, 3)
    N = len(source)

    for i in range(maxIter):
        print(i)
        source_tr = np.matmul(source, A)
        phi = map_points(source_tr, target)

        trimN = int(np.floor(N*trim))

        phi_sorted = sorted(phi, key=dist)
        phi_tr = phi_sorted[:trimN]

        src_idxs = list(map(idx1, phi_tr))
        trg_idxs = list(map(idx2, phi_tr))

        A1, _, _, _ = lstsq(source_tr[src_idxs], target[trg_idxs])

        # d = np.abs(np.sum(A1 - A))
        d = sum(list(map(dist, map_points(np.matmul(source, A1), target))))
        print(d)

        if d < eps:
            break
        else:
            A = A1

    return A


def mesh_dist(m1, m2):
    src_ps = m1.points
    target_ps = m2.points

    n_points = len(m1.points)
    
    d = sum(list(map(dist, map_points(src_ps, target_ps))))

    return d / n_points


def plot_dist(m1, m2):
    m1.points = centre_points(m1.points)
    m2.points = centre_points(m2.points)
    s = m1.points
    t = m2.points

    ds = list(map(dist, map_points(s, t)))

    plotter = pv.Plotter()
    plotter.add_mesh(m1, scalars=ds)

    plotter.show(screenshot='~/flower-experiments/dist-RK.png')
