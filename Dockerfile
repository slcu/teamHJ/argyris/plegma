FROM python:3.6

RUN apt-get update && apt-get install -y python3-pip

COPY requirements.txt .

RUN pip install -r requirements.txt

# Install jupyter
RUN pip3 install ipython
RUN pip3 install cython
RUN apt-get install -y python-qt4 libgl1-mesa-glx

# Create a new system user
RUN useradd -ms /bin/bash demo

# Change to this new user
USER demo

ADD ./common/ /home/common/

# Set the container working directory to the user home folder
WORKDIR /home/plegma/

RUN pip3 install /home/common

ENTRYPOINT ["ipython"]

#docker build . -t plegma
#docker run -it --mount type=bind,source="$(pwd)",target=/home/plegma/ plegma
